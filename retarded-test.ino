#include <Wire.h>
#include <MCUFRIEND_kbv.h>
#include "TouchScreen_kbv.h"
#include <iarduino_RTC.h>
#include "DS1307.h"

#define YP A2  // LCD_WR must be an analog pin, use "An" notation!
#define XM A3  // LCD_RS must be an analog pin, use "An" notation!
#define YM 8   // LCD_D7 can be a digital pin
#define XP 9   // LCD_D6 can be a digital pin

#define LOG(fmt, ...) \
{ \
  static char buf__LINE__[128]; \
  sprintf(&buf__LINE__[0], fmt, ##__VA_ARGS__); \
  Serial.println(buf__LINE__);\
  if (tft != nullptr) { \
    tft->println(buf__LINE__);\
  } \
}

#define ERR(fmt, ...) \
{ \
  static char buf__LINE__[128]; \
  sprintf(&buf__LINE__[0], fmt, ##__VA_ARGS__); \
  Serial.println(buf__LINE__); \
  exitError(buf__LINE__); \
}

static const int memAddress = 0x50;
static const int timeAddress = 0x68;

static TouchScreen_kbv *ts = nullptr;
static MCUFRIEND_kbv *tft = nullptr;

void exitError(const char *str)
{
  Serial.println("FAILED");
  if (tft != nullptr)
  {
    tft->println(str);
  }
  
  for(;;)
  {
    TSPoint_kbv p = ts->getPoint();
    // these lines are crucial for the normal display work
    // restore display output pins mode
    pinMode(YP, OUTPUT);
    pinMode(XM, OUTPUT);
  
    if (p.z > 200)
    {
      tft->fillScreen(TFT_RED);
      break;
    }
  }
  for(;;){}
}

void OK()
{
  Serial.println("PASSED");
  if (tft != nullptr)
  {
    tft->fillScreen(TFT_GREEN);
  }
}

int testI2Caddress(int address)
{
  Wire.beginTransmission(address);
  return Wire.endTransmission();
}

void checkI2C()
{
  LOG("checking i2c devices");
  Serial.println("Checking I2C devices");
  byte error, address;
  int nDevices;

  Serial.println("Scanning...");

  bool memFound = false;
  bool timeFound = false;
  nDevices = 0;
  for(address = 8; address < 127; address++ ){
    error = testI2Caddress(address);

    if (error == 0){
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println(" !");
      if (address == memAddress) {
        memFound = true;
      }
      if (address == timeAddress) {
        timeFound = true;
      }
      nDevices++;
    }
    else if (error==4) {
      Serial.print("Unknow error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    } 
  }
  if (nDevices == 0) {
    ERR("No I2C devices found");
  } else if (nDevices == 2) {
    Serial.println("i2c check done");
  } else {
    if (!timeFound) {
      ERR("No time device found");
    }
    if (!memFound) {
      ERR("No memory device found");
    }
  }
  LOG("i2c devices OK");
}

bool checkDisplay()
{
  tft = new MCUFRIEND_kbv();
  tft->reset();
  uint16_t identifier = tft->readID();
  if(identifier == 0x9325) {
    Serial.println("Found ILI9325 LCD driver");
  } else if(identifier == 0x9328) {
    Serial.println("Found ILI9328 LCD driver");
  } else if(identifier == 0x4535) {
    Serial.println("Found LGDP4535 LCD driver");
  }else if(identifier == 0x7575) {
    Serial.println("Found HX8347G LCD driver");
  } else if(identifier == 0x9595) {
    Serial.println("Found HX8347-I LCD driver");
  } else if(identifier == 0x4747) {
    Serial.println("Found HX8347-D LCD driver");
  } else if(identifier == 0x8347) {
    Serial.println("Found HX8347-A LCD driver");
  }else if(identifier == 0x9341) {
    Serial.println("Found ILI9341 LCD driver");
  }else if(identifier == 0x7783) {
    Serial.println("Found ST7781 LCD driver");
  }else if(identifier == 0x8230) {
    Serial.println("Found UC8230 LCD driver");  
  }else if(identifier == 0x8357) {
    Serial.println("Found HX8357D LCD driver");
  } else if(identifier==0x0101){     
      identifier=0x9341;
      Serial.println("Found 0x9341 LCD driver");
  }else if(identifier==0x7793){     
       Serial.println("Found ST7793 LCD driver");
  }else if(identifier==0xB509){     
       Serial.println("Found R61509 LCD driver");
  }else if(identifier==0x9486){     
       Serial.println("Found ILI9486 LCD driver");
  }else if(identifier==0x9488){     
       Serial.println("Found ILI9488 LCD driver");
  }else {
    Serial.print("Unknown LCD driver chip: ");
    Serial.println(identifier, HEX);
    Serial.println("If using the Adafruit 2.8\" TFT Arduino shield, the line:");
    Serial.println("  #define USE_ADAFRUIT_SHIELD_PINOUT");
    Serial.println("should appear in the library header (Adafruit_TFT.h).");
    Serial.println("If using the breakout board, it should NOT be #defined!");
    Serial.println("Also if using the breakout, double-check that all wiring");
    Serial.println("matches the tutorial.");
    tft = nullptr;
    ERR("Unknown LCD driver chip id %d", identifier);
    identifier=0x9486;
  }
  tft->begin(identifier);
  tft->setRotation(1);
  tft->setTextSize(2);
  tft->setTextColor(TFT_BLACK, TFT_WHITE);
  tft->fillRect(0,0,tft->width()/2,tft->height()/2, TFT_WHITE);
  tft->fillRect(tft->width()/2,0,tft->width()/2,tft->height()/2, TFT_BLUE);
  tft->fillRect(tft->width()/2,tft->height()/2,tft->width()/2,tft->height()/2, TFT_BLACK);
  tft->fillRect(0,tft->height()/2,tft->width()/2,tft->height()/2, TFT_CYAN);
  tft->fillScreen(TFT_YELLOW);
  LOG("TFT initialized");
}

void checkTime()
{
  LOG("checking rtc timer");
  DS1307 rtcTimer;
  rtcTimer.begin();
  rtcTimer.fillByHMS(15, 28, 30); //15:28 30"
  rtcTimer.setTime();
  rtcTimer.getTime();
  int seconds = rtcTimer.second;
  int mins = rtcTimer.minute;
  int hours = rtcTimer.hour;
  LOG("start %d:%d:%d", hours, mins, seconds);

  int refTime = millis();
  for (int i = 0 ; i != 10 ; ++i)
  { 
    rtcTimer.getTime();
    int spent = rtcTimer.hour * 3600 + rtcTimer.minute * 60 + rtcTimer.second - (hours * 3600 + mins * 60 + seconds);
    seconds = rtcTimer.second;
    mins = rtcTimer.minute;
    hours = rtcTimer.hour;
  
    int refSpent = millis() - refTime;
    static const int delta = 1010;
    spent *= 1000;
    if (spent < (refSpent - delta))
    {
      LOG("%d:%d:%d %d ref %d", hours, mins, seconds, spent, refSpent);
      ERR("time underflow %d ref %d", spent, refSpent);
    }
    if (spent > (refSpent + delta))
    {
      LOG("%d:%d:%d %d ref %d", hours, mins, seconds, spent, refSpent);
      ERR("time overflow %d ref %d", spent, refSpent);
    }
    refTime = millis();
    delay(2000);
  }
  LOG("rtc timer OK");
  rtcTimer.fillByHMS(0, 0, 0);
  rtcTimer.setTime();

  for (int i = 0 ; i != 1000 ; ++i)
  {
    int error = testI2Caddress(timeAddress);
    if (error != 0){
      ERR("time i2c failed recheck")
    }
  }
  LOG("i2c time recheck OK");
}

void checkTouchScreen()
{
  LOG("checking touchscreen");
  ts = new TouchScreen_kbv(XP, YP, XM, YM, 300);
  LOG("touch screen please");

  for(;;)
  {
    TSPoint_kbv p = ts->getPoint();
    // these lines are crucial for the normal display work
    // restore display output pins mode
    pinMode(YP, OUTPUT);
    pinMode(XM, OUTPUT);
  
    if (p.z > 200)
    {
      break;
    }
  }
  
  LOG("touchscreen OK");
}

void setup()
{
  Wire.begin();    
  Serial.begin(9600);
  while (!Serial);

  checkDisplay();
  checkTouchScreen();
  checkI2C();
  checkTime();
  OK();
}

void loop()
{
}
